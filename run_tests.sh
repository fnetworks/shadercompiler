#!/bin/sh
jabs_run=${jabs_run:-jabs run --}
for f in test_suite/*.shader; do
	echo "$f" | sed s/test_suite\\///
	$jabs_run "$f" > test_out.txt || exit 1
	diff test_out.txt "$(echo "$f" | sed s/\\.shader$/.expected/)" || exit 1
done
rm -f test_out.txt
