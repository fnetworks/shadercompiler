# Shadercompiler

Shadercompiler is a transpiler that plans to transpile a common shader language
to other languages like GLSL, HLSL or maybe even SPIR-V.

## Building

1. Download the JABS executable from
   <https://gitlab.com/jabs-build/jabs/-/jobs/artifacts/master/raw/target/release/jabs?job=build>
   (currently only linux binary)
2. To build, run ``jabs build``
3. To run the compiler, run `jabs run -- <file_name>`, where `<file_name>` is
   the file to compile.
