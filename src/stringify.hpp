#pragma once

#include "visitor.hpp"

#include <sstream>

class StringifyVisitor : public ConstVisitor {
private:
	std::stringstream output{};

	uint8_t m_indent{0};

	inline std::string indent() const {
		std::string s;
		s.reserve(m_indent * 2);
		for (uint8_t i = 0; i < m_indent; i++)
			s += "  ";
		return s;
	}

public:
	inline std::string str() const {
		return output.str();
	}

	void visit_integer(const ast::Integer& n) override;
	void visit_binary_op(const ast::BinaryOp& n) override;
	void visit_assignment(const ast::Assignment& n) override;
	void visit_variable(const ast::Variable& n) override;
	void visit_function_call(const ast::FunctionCall& n) override;
	void visit_basic_block(const ast::BasicBlock& n) override;
	void visit_variable_decl(const ast::VariableDecl& n) override;
	void visit_function_decl(const ast::FunctionDecl& n) override;
	void visit_compilation_unit(const ast::CompilationUnit& n) override;
};
