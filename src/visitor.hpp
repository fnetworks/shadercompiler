#pragma once

#include <memory>

namespace ast {

	class Node;
	class Integer;
	class BinaryOp;
	class Assignment;
	class Variable;
	class FunctionCall;
	class BasicBlock;
	class VariableDecl;
	class FunctionDecl;
	class CompilationUnit;

}

class ConstVisitor {
public:
	virtual void visit_integer(const ast::Integer&);
	virtual void visit_binary_op(const ast::BinaryOp&);
	virtual void visit_assignment(const ast::Assignment&);
	virtual void visit_variable(const ast::Variable&);
	virtual void visit_function_call(const ast::FunctionCall&);
	virtual void visit_basic_block(const ast::BasicBlock&);
	virtual void visit_variable_decl(const ast::VariableDecl&);
	virtual void visit_function_decl(const ast::FunctionDecl&);
	virtual void visit_compilation_unit(const ast::CompilationUnit&);
};

class Visitor {
public:
	virtual void visit_integer(ast::Integer&);
	virtual void visit_binary_op(ast::BinaryOp&);
	virtual void visit_assignment(ast::Assignment&);
	virtual void visit_variable(ast::Variable&);
	virtual void visit_function_call(ast::FunctionCall&);
	virtual void visit_basic_block(ast::BasicBlock&);
	virtual void visit_variable_decl(ast::VariableDecl&);
	virtual void visit_function_decl(ast::FunctionDecl&);
	virtual void visit_compilation_unit(ast::CompilationUnit&);
};

class ModifyVisitor {
public:
	virtual void visit_integer(ast::Integer&, std::unique_ptr<ast::Node>& owner);
	virtual void visit_binary_op(ast::BinaryOp&, std::unique_ptr<ast::Node>& owner);
	virtual void visit_assignment(ast::Assignment&, std::unique_ptr<ast::Node>& owner);
	virtual void visit_variable(ast::Variable&, std::unique_ptr<ast::Node>& owner);
	virtual void visit_function_call(ast::FunctionCall&, std::unique_ptr<ast::Node>& owner);
	virtual void visit_basic_block(ast::BasicBlock&, std::unique_ptr<ast::Node>& owner);
	virtual void visit_variable_decl(ast::VariableDecl&, std::unique_ptr<ast::Node>& owner);
	virtual void visit_function_decl(ast::FunctionDecl&, std::unique_ptr<ast::Node>& owner);
	virtual void visit_compilation_unit(ast::CompilationUnit&, std::unique_ptr<ast::Node>& owner);
};
