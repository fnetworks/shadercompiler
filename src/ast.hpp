#pragma once

#include <string>
#include <memory>
#include <optional>
#include <vector>

#include "visitor.hpp"

namespace ast {

template<typename T>
using Owned = std::unique_ptr<T>;

#define dispatch_fn(visit) \
	void dispatch(Visitor& visitor) override { \
		visitor.visit(*this); \
	} \
	void dispatch(ConstVisitor& visitor) const override { \
		visitor.visit(*this); \
	} \
	void dispatch(ModifyVisitor& visitor, std::unique_ptr<Node>& owner) override {\
		visitor.visit(*this, owner); \
	}

class Node {
public:
	virtual ~Node() = default;

	virtual void dispatch(Visitor& visitor) = 0;
	virtual void dispatch(ConstVisitor& visitor) const = 0;
	virtual void dispatch(ModifyVisitor& visitor, std::unique_ptr<Node>& owner) = 0;
};

class Integer : public Node {
public:
	using value_type = unsigned int;

private:
	value_type m_value;

public:
	explicit inline Integer(value_type value)
		: m_value(value) {}

	constexpr value_type value() const { return m_value; }

	dispatch_fn(visit_integer)
};

class Variable : public Node {
private:
	std::string m_name; // TODO Atom

public:
	explicit inline Variable(std::string name)
		: m_name(std::move(name)) {}

	constexpr const std::string& name() const { return m_name; }

	dispatch_fn(visit_variable)
};

class BinaryOp : public Node {
private:
	Owned<Node> m_left;
	Owned<Node> m_right;
	char m_operator;
	// bool m_paren_op;

public:
	inline BinaryOp(Owned<Node> left, Owned<Node> right, char op)
		: m_left(std::move(left)), m_right(std::move(right)), m_operator(op) {}

	constexpr const Owned<Node>& left() const { return m_left; }
	constexpr Owned<Node>& left() { return m_left; }

	constexpr const Owned<Node>& right() const { return m_right; }
	constexpr Owned<Node>& right() { return m_right; }

	constexpr char get_operator() const { return m_operator; }

	dispatch_fn(visit_binary_op)
};

class FunctionCall : public Node {
private:
	std::string m_name; // TODO Atom
	std::vector<Owned<Node>> m_arguments;

public:
	explicit inline FunctionCall(std::string name, std::vector<Owned<Node>> arguments)
		: m_name(std::move(name)), m_arguments(std::move(arguments)) {}

	constexpr const std::string& name() const { return m_name; }
	constexpr const std::vector<Owned<Node>>& arguments() const { return m_arguments; }
	constexpr std::vector<Owned<Node>>& arguments() { return m_arguments; }

	dispatch_fn(visit_function_call)
};

class Assignment : public Node {
private:
	std::string m_left; // TODO Atom
	Owned<Node> m_right;

public:
	inline Assignment(std::string left, Owned<Node> right)
		: m_left(std::move(left)), m_right(std::move(right)) {}

	constexpr const std::string& left() const { return m_left; }
	
	constexpr const Owned<Node>& right() const { return m_right; }
	constexpr Owned<Node>& right() { return m_right; }

	dispatch_fn(visit_assignment)
};

class VariableDecl : public Node {
private:
	std::string m_name; // TODO Atom
	std::optional<Owned<Node>> m_value;

public:
	inline VariableDecl(std::string name, std::optional<Owned<Node>> value)
		: m_name(std::move(name)), m_value(std::move(value)) {}

	constexpr const std::string& name() const { return m_name; }

	constexpr const std::optional<Owned<Node>>& value() const { return m_value; }
	constexpr std::optional<Owned<Node>>& value() { return m_value; }

	dispatch_fn(visit_variable_decl)
};


class BasicBlock : public Node {
private:
	std::vector<Owned<Node>> m_statements;

public:
	inline explicit BasicBlock(std::vector<Owned<Node>> statements)
		: m_statements(std::move(statements)) {}

	constexpr const decltype(m_statements)& statements() const { return m_statements; }
	constexpr decltype(m_statements)& statements() { return m_statements; }
	
	dispatch_fn(visit_basic_block)
};

class FunctionDecl : public Node {
private:
	std::string m_name;
	Owned<Node> m_content;

public:
	inline FunctionDecl(std::string name, Owned<Node> content)
		: m_name(std::move(name)), m_content(std::move(content)) {}

	constexpr const std::string& name() const { return m_name; }
	constexpr const Owned<Node>& content() const { return m_content; }
	constexpr Owned<Node>& content() { return m_content; }

	dispatch_fn(visit_function_decl)
};

class CompilationUnit : public Node {
private:
	std::vector<Owned<Node>> m_functions;

public:
	inline explicit CompilationUnit(std::vector<Owned<Node>> functions)
		: m_functions(std::move(functions)) {}

	constexpr const std::vector<Owned<Node>>& functions() const { return m_functions; }
	constexpr std::vector<Owned<Node>>& functions() { return m_functions; }

	dispatch_fn(visit_compilation_unit)
};

}
