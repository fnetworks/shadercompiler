#include "visitor.hpp"
#include "ast.hpp"

void ConstVisitor::visit_integer(const ast::Integer&) {}

void ConstVisitor::visit_binary_op(const ast::BinaryOp& n) {
	n.left()->dispatch(*this);
	n.right()->dispatch(*this);
}

void ConstVisitor::visit_assignment(const ast::Assignment& n) {
	n.right()->dispatch(*this);
}

void ConstVisitor::visit_variable(const ast::Variable&) {}

void ConstVisitor::visit_function_call(const ast::FunctionCall& n) {
	for (const auto& arg : n.arguments())
		arg->dispatch(*this);
}

void ConstVisitor::visit_basic_block(const ast::BasicBlock& n) {
	for (const auto& stmt : n.statements())
		stmt->dispatch(*this);
}

void ConstVisitor::visit_variable_decl(const ast::VariableDecl& n) {
	if (n.value())
		(*n.value())->dispatch(*this);
}

void ConstVisitor::visit_function_decl(const ast::FunctionDecl& n) {
	n.content()->dispatch(*this);
}

void ConstVisitor::visit_compilation_unit(const ast::CompilationUnit& n) {
	for (auto& fn : n.functions())
		fn->dispatch(*this);
}

void Visitor::visit_integer(ast::Integer&) {}

void Visitor::visit_binary_op(ast::BinaryOp& n) {
	n.left()->dispatch(*this);
	n.right()->dispatch(*this);
}

void Visitor::visit_assignment(ast::Assignment& n) {
	n.right()->dispatch(*this);
}

void Visitor::visit_variable(ast::Variable&) {}

void Visitor::visit_function_call(ast::FunctionCall& n) {
	for (auto& arg : n.arguments())
		arg->dispatch(*this);
}

void Visitor::visit_basic_block(ast::BasicBlock& n) {
	for (auto& stmt : n.statements())
		stmt->dispatch(*this);
}

void Visitor::visit_variable_decl(ast::VariableDecl& n) {
	if (n.value())
		(*n.value())->dispatch(*this);
}

void Visitor::visit_function_decl(ast::FunctionDecl& n) {
	n.content()->dispatch(*this);
}

void Visitor::visit_compilation_unit(ast::CompilationUnit& n) {
	for (auto& fn : n.functions())
		fn->dispatch(*this);
}


void ModifyVisitor::visit_integer(ast::Integer&, std::unique_ptr<ast::Node>&) {}

void ModifyVisitor::visit_binary_op(ast::BinaryOp& n, std::unique_ptr<ast::Node>&) {
	n.left()->dispatch(*this, n.left());
	n.right()->dispatch(*this, n.right());
}

void ModifyVisitor::visit_assignment(ast::Assignment& n, std::unique_ptr<ast::Node>&) {
	n.right()->dispatch(*this, n.right());
}

void ModifyVisitor::visit_variable(ast::Variable&, std::unique_ptr<ast::Node>&) {}

void ModifyVisitor::visit_function_call(ast::FunctionCall& n, std::unique_ptr<ast::Node>&) {
	for (auto& arg : n.arguments())
		arg->dispatch(*this, arg);
}

void ModifyVisitor::visit_basic_block(ast::BasicBlock& n, std::unique_ptr<ast::Node>&) {
	for (auto& stmt : n.statements())
		stmt->dispatch(*this, stmt);
}

void ModifyVisitor::visit_variable_decl(ast::VariableDecl& n, std::unique_ptr<ast::Node>&) {
	if (n.value()) {
		auto& value = *n.value();
		value->dispatch(*this, value);
	}		
}

void ModifyVisitor::visit_function_decl(ast::FunctionDecl& n, std::unique_ptr<ast::Node>&) {
	n.content()->dispatch(*this, n.content());
}

void ModifyVisitor::visit_compilation_unit(ast::CompilationUnit& n, std::unique_ptr<ast::Node>&) {
	for (auto& fn : n.functions())
		fn->dispatch(*this, fn);
}
