#include "../visitor.hpp"

class ConstEvalOptimizer : public ModifyVisitor {
public:
	virtual void visit_binary_op(ast::BinaryOp&, std::unique_ptr<ast::Node>&) override;
};
