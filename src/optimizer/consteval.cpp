#include "consteval.hpp"
#include "../ast.hpp"

#include <iostream>

void ConstEvalOptimizer::visit_binary_op(ast::BinaryOp& n, std::unique_ptr<ast::Node>& node) {
	n.left()->dispatch(*this, n.left());
	n.right()->dispatch(*this, n.right());
	if (auto* left = dynamic_cast<ast::Integer*>(n.left().get())) {
		if (auto* right = dynamic_cast<ast::Integer*>(n.right().get())) {
			unsigned int left_val = left->value();
			unsigned int right_val = right->value();
			unsigned int result = 0;
			switch (n.get_operator()) {
				case '+': result = left_val + right_val; break;
				case '-': result = left_val - right_val; break;
				case '*': result = left_val * right_val; break;
				case '/': result = left_val / right_val; break;
				default: return;
			}
			node = std::make_unique<ast::Integer>(result);
		}
	}
}
