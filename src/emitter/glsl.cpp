#include "glsl.hpp"
#include "../ast.hpp"

void GLSLVisitor::visit_integer(const ast::Integer& n) {
	output << std::to_string(n.value());
}

void GLSLVisitor::visit_binary_op(const ast::BinaryOp& n) {
	n.left()->dispatch(*this);
	output << ' ' << n.get_operator() << ' ';
	n.right()->dispatch(*this);
}

void GLSLVisitor::visit_assignment(const ast::Assignment& n) {
	output << n.left() << " = ";
	n.right()->dispatch(*this);
}

void GLSLVisitor::visit_variable(const ast::Variable& n) {
	output << n.name();
}

void GLSLVisitor::visit_function_call(const ast::FunctionCall& n) {
	output << n.name() << '(';
	const auto& args = n.arguments();
	for (size_t i = 0; i < args.size(); i++) {
		args[i]->dispatch(*this);
		if (i < args.size() - 1)
			output << ", ";
	}
	output << ')';
}

void GLSLVisitor::visit_basic_block(const ast::BasicBlock& n) {
	output << "{\n";
	m_indent++;
	for (const auto& stmt : n.statements()) {
		output << indent();
		stmt->dispatch(*this);
		output << ";\n";
	}
	m_indent--;
	output << '}';
}

void GLSLVisitor::visit_variable_decl(const ast::VariableDecl& n) {
	output << "int " << n.name();
	if (n.value()) {
		output << " = ";
		(*n.value())->dispatch(*this);
	}
}

void GLSLVisitor::visit_function_decl(const ast::FunctionDecl& n) {
	output << "void " << n.name() << "() ";
	n.content()->dispatch(*this);
	output << '\n';
}

void GLSLVisitor::visit_compilation_unit(const ast::CompilationUnit& n) {
	output << "#version 320 core\n\n";
	for (const auto& fn : n.functions())
		fn->dispatch(*this);
}
