#include "token_stream.hpp"

#include <sstream>

const Token& TokenStream::peek(size_t skip) const {
	auto index = m_index + skip;
	if (index >= m_tokens.size())
		return m_eof_token;
	return m_tokens[index];
}

Token TokenStream::expect(TokenType type) {
	auto token = next();
	if (token.type() != type) {
		std::stringstream ss;
		ss << "Expected " << type << ", but got " << token.type();
		throw std::runtime_error(ss.str());
	}
	return token;
}

Token TokenStream::next() {
	const auto& next = peek();
	m_index++;
	return next;
}
