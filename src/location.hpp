#pragma once

#include <string>
#include <ostream>

struct Location {
public:
	using position_type = unsigned int;

private:
	position_type m_line;
	position_type m_column;

public:
	constexpr Location(unsigned int line, unsigned int column)
		: m_line(line), m_column(column) {}

	constexpr position_type line() const { return m_line; }
	constexpr position_type column() const { return m_column; }

	constexpr void next_line() { m_line++; m_column = 0; }
	constexpr void next_column() { m_column++; }

	inline std::string to_string() const {
		return std::to_string(m_line) + ':' + std::to_string(m_column);
	}
};

constexpr Location operator+(const Location& lhs, Location::position_type rhs) {
	return Location(lhs.line(), lhs.column() + rhs);
}

constexpr Location operator-(const Location& lhs, Location::position_type rhs) {
	return Location(lhs.line(), lhs.column() - rhs);
}

inline std::ostream& operator<<(std::ostream& out, const Location& loc) {
	return out << loc.line() << ':' << loc.column();
}

