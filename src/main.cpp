#include <iostream>
#include <fstream>
#include <vector>
#include <string>
using namespace std::string_literals;
#include <filesystem>
namespace fs = std::filesystem;

#include "tokenizer.hpp"
#include "token_stream.hpp"
#include "parser.hpp"
#include "emitter/glsl.hpp"
#include "optimizer/consteval.hpp"

static std::string read_string(const fs::path& file) {
	std::ifstream i(file);
	i.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	i.seekg(0, std::ios::end);
	size_t size = i.tellg();
	i.seekg(0);
	std::string buffer(size, '\0');
	i.read(buffer.data(), size);
	return buffer;
}

int main(int argc, char** argv) {
	if (argc <= 1) {
		std::cout << "Usage: shadercompiler <file>\n";
		return EXIT_FAILURE;
	}

	fs::path file(argv[1]);
	auto tokens = tokenize(read_string(file));
	TokenStream stream(tokens);
	Parser parser(stream);
	ast::Owned<ast::Node> it = parser.parse_compilation_unit();
	ConstEvalOptimizer opt;
	it->dispatch(opt, it);
	GLSLVisitor visitor;
	it->dispatch(visitor);
	std::cout << visitor.str() << '\n';
	return EXIT_SUCCESS;
}
