#pragma once

#include <string_view>
#include <string>
using namespace std::string_literals;
#include <ostream>
#include <variant>
#include <vector>
#include <sstream>

#include "location.hpp"

enum class TokenType {
	Identifier,
	Integer,

	Let,
	Void,
	For,
	If,

	LParen,
	RParen,
	LBracket,
	RBracket,
	LCurly,
	RCurly,

	Comma,
	Semicolon,
	Plus,
	Minus,
	Star,
	Slash,
	Equals,

	EndOfFile
};

constexpr std::string_view token_type_to_string(TokenType t) {
	switch (t) {
		case TokenType::Identifier: return "identifier";
		case TokenType::Integer: return "integer";
		case TokenType::LParen: return "lparen";
		case TokenType::RParen: return "rparen";
		case TokenType::LBracket: return "lbracket";
		case TokenType::RBracket: return "rbracket";
		case TokenType::LCurly: return "lcurly";
		case TokenType::RCurly: return "rcurly";
		case TokenType::Comma: return "comma";
		case TokenType::Semicolon: return "semicolon";
		case TokenType::Plus: return "plus";
		case TokenType::Minus: return "minus";
		case TokenType::Star: return "star";
		case TokenType::Slash: return "slash";
		case TokenType::Equals: return "equals";
		case TokenType::EndOfFile: return "eof";
		default: return "<unknown>";
	}
}

inline std::ostream& operator<<(std::ostream& out, TokenType type) {
	return out << token_type_to_string(type);
}

struct Token {
public:
	using value_type = std::variant<std::monostate, std::string, unsigned int, char>;
private:
	TokenType m_type;

	Location m_start;
	Location m_end;

	value_type m_value;

public:
	constexpr Token(TokenType type, Location start, Location end)
		: m_type(type), m_start(std::move(start)), m_end(std::move(end)), m_value{} {}

	template<typename T>
	inline Token(TokenType type, Location start, Location end, T value)
		: m_type(type), m_start(std::move(start)), m_end(std::move(end)), m_value(std::move(value)) {}

	constexpr Token(TokenType type, Location single)
		: m_type(type), m_start(single), m_end(single), m_value{} {}

	template<typename T>
	inline Token(TokenType type, Location single, T value)
		: m_type(type), m_start(single), m_end(single), m_value(std::move(value)) {}

	constexpr const Location& start() const { return m_start; }
	constexpr const Location& end() const { return m_end; }
	constexpr TokenType type() const { return m_type; }
	
	constexpr bool value_is_empty() const { return std::holds_alternative<std::monostate>(m_value); }
	constexpr const value_type& value() const { return m_value; }
	constexpr bool value_is_string() const { return std::holds_alternative<std::string>(m_value); }
	constexpr const std::string& value_as_string() const { return std::get<std::string>(m_value); }
	constexpr bool value_is_uint() const { return std::holds_alternative<unsigned int>(m_value); }
	constexpr unsigned int value_as_uint() const { return std::get<unsigned int>(m_value); }
	constexpr bool value_is_char() const { return std::holds_alternative<char>(m_value); }
	constexpr char value_as_char() const { return std::get<char>(m_value); }
	
	inline std::string to_string() const;

	static inline Token eof(Location at) {
		return Token(TokenType::EndOfFile, at);
	}
};

inline std::ostream& operator<<(std::ostream& out, const Token& token) {
	out << token.type();
	if (!token.value_is_empty()) {
		out << '(';
		if (token.value_is_string())
			out << token.value_as_string();
		else if (token.value_is_uint())
			out << token.value_as_uint();
		else if (token.value_is_char())
			out << token.value_as_char();
		else
			out << "<unknown>";
		out << ')';
	}
	return out;
}
std::string Token::to_string() const {
	std::stringstream ss;
	ss << *this;
	return ss.str();
}

std::vector<Token> tokenize(const std::string& str); 
