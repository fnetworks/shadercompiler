#pragma once

#include "token_stream.hpp"
#include "ast.hpp"

class Parser {
private:
	TokenStream m_tokens;

public:
	explicit Parser(TokenStream tokens)
		: m_tokens(std::move(tokens)) {}

	ast::Owned<ast::Node> parse_expr(unsigned int precedence = 1);
	ast::Owned<ast::Node> parse_term();
	std::vector<ast::Owned<ast::Node>> parse_argument_list();
	ast::Owned<ast::Node> parse_function_call();
	ast::Owned<ast::Node> parse_assignment();
	ast::Owned<ast::Node> parse_variable_decl();
	ast::Owned<ast::Node> parse_statement();
	ast::Owned<ast::Node> parse_block();
	ast::Owned<ast::Node> parse_function_decl();
	ast::Owned<ast::Node> parse_compilation_unit();
};
