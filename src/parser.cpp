#include "parser.hpp"

#include <map>
#include <iostream>

namespace {

	constexpr bool is_left_associative(char op) {
		switch (op) {
			case '+':
			case '-':
			case '*':
			case '/':
				return true;
			default: throw std::runtime_error("Invalid operator "s + op);
		}
	}

	constexpr unsigned int precedence(char op) {
		switch (op) {
			case '+':
			case '-':
				return 1;
			case '*':
			case '/':
				return 2;
			default: throw std::runtime_error("Invalid operator "s + op);
		}
	}

	constexpr bool is_operator(TokenType t) {
		return t == TokenType::Plus
			|| t == TokenType::Minus
			|| t == TokenType::Star
			|| t == TokenType::Slash;
	}
	
}

ast::Owned<ast::Node> Parser::parse_expr(unsigned int prec) {
	ast::Owned<ast::Node> lhs = parse_term();
	while (true) {
		const auto& cur = m_tokens.peek();
		if (!is_operator(cur.type())) break;
		auto op = cur.value_as_char();
		auto cur_prec = precedence(op);
		if (cur_prec < prec) break;
		m_tokens.consume();
		auto rhs = parse_expr(cur_prec + (is_left_associative(op) ? 1 : 0));
		lhs = std::make_unique<ast::BinaryOp>(std::move(lhs), std::move(rhs), op);
	}
	return lhs;
}

ast::Owned<ast::Node> Parser::parse_term() {
	const auto& tok = m_tokens.peek();
	if (tok.type() == TokenType::LParen) {
		m_tokens.consume();
		auto expr = parse_expr();
		m_tokens.expect(TokenType::RParen);
		return expr;
	} else if (tok.type() == TokenType::Integer) {
		m_tokens.consume();
		return std::make_unique<ast::Integer>(tok.value_as_uint());
	} else if (tok.type() == TokenType::Identifier) {
		if (m_tokens.peek(1).type() == TokenType::LParen) {
			return parse_function_call();
		} else {
			m_tokens.consume();
			return std::make_unique<ast::Variable>(tok.value_as_string());
		}
	} else {
		throw std::runtime_error("Unexpected "s + tok.to_string()
				+ ", expected lparen, integer or identifier at "s + tok.start().to_string());
	}
}

ast::Owned<ast::Node> Parser::parse_function_call() {
	auto ident = m_tokens.expect(TokenType::Identifier);
	m_tokens.expect(TokenType::LParen);
	auto args = parse_argument_list();
	return std::make_unique<ast::FunctionCall>(ident.value_as_string(), std::move(args));
}

std::vector<ast::Owned<ast::Node>> Parser::parse_argument_list() {
	std::vector<ast::Owned<ast::Node>> arguments;
	while (true) {
		const auto& maybe_arg = m_tokens.peek();
		if (maybe_arg.type() == TokenType::RParen) break;
		arguments.push_back(parse_expr());
		const auto& maybe_comma = m_tokens.peek();
		if (maybe_comma.type() == TokenType::RParen) break;
		m_tokens.expect(TokenType::Comma);
	}
	m_tokens.expect(TokenType::RParen);
	return arguments;
}

ast::Owned<ast::Node> Parser::parse_assignment() {
	auto ident = m_tokens.expect(TokenType::Identifier);
	m_tokens.expect(TokenType::Equals);
	auto rhs = parse_expr();
	return std::make_unique<ast::Assignment>(ident.value_as_string(), std::move(rhs));
}

ast::Owned<ast::Node> Parser::parse_block() {
	m_tokens.expect(TokenType::LCurly);
	std::vector<ast::Owned<ast::Node>> statements;
	while (true) {
		const auto& maybe_next = m_tokens.peek();
		if (maybe_next.type() == TokenType::RCurly) break;
		statements.push_back(parse_statement());
		m_tokens.expect(TokenType::Semicolon);
	}
	m_tokens.expect(TokenType::RCurly);
	return std::make_unique<ast::BasicBlock>(std::move(statements));
}

ast::Owned<ast::Node> Parser::parse_statement() {
	const auto& a = m_tokens.peek();
	if (a.type() == TokenType::Identifier) {
		if (m_tokens.peek(1).type() == TokenType::LParen)
			return parse_function_call();
		return parse_assignment();
	} else if (a.type() == TokenType::LCurly) {
		return parse_block();
	} else if (a.type() == TokenType::Let) {
		return parse_variable_decl();
	} else {
		throw std::runtime_error("Unexpected "s + a.to_string() + ", expected identifier at "s + a.start().to_string());
	}
}

ast::Owned<ast::Node> Parser::parse_variable_decl() {
	m_tokens.expect(TokenType::Let);
	auto ident = m_tokens.expect(TokenType::Identifier);
	std::optional<ast::Owned<ast::Node>> value;
	const auto& maybe_equals = m_tokens.peek();
	if (maybe_equals.type() == TokenType::Equals) {
		m_tokens.consume();
		value = parse_expr();
	}
	return std::make_unique<ast::VariableDecl>(ident.value_as_string(), std::move(value));
}

ast::Owned<ast::Node> Parser::parse_function_decl() {
	m_tokens.expect(TokenType::Void);
	auto ident = m_tokens.expect(TokenType::Identifier);
	m_tokens.expect(TokenType::LParen);
	m_tokens.expect(TokenType::RParen);
	auto content = parse_block();
	return std::make_unique<ast::FunctionDecl>(ident.value_as_string(), std::move(content));
}

ast::Owned<ast::Node> Parser::parse_compilation_unit() {
	const Token* next = &m_tokens.peek();
	std::vector<ast::Owned<ast::Node>> functions;
	while (next->type() != TokenType::EndOfFile) {
		functions.push_back(parse_function_decl());
		next = &m_tokens.peek();
	}
	m_tokens.expect(TokenType::EndOfFile);
	return std::make_unique<ast::CompilationUnit>(std::move(functions));
}
