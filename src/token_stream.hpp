#pragma once

#include <vector>
#include <cstddef>

#include "tokenizer.hpp"

class TokenStream {
private:
	std::vector<Token> m_tokens;
	size_t m_index;
	Token m_eof_token;

public:
	inline Location eof_loc() const {
		if (m_tokens.size() > 0)
			return m_tokens[m_tokens.size() - 1].end() + 1;
		return Location(0, 0);
	}

	explicit inline TokenStream(std::vector<Token> tokens)
		: m_tokens(std::move(tokens)), m_index(0), m_eof_token(Token(TokenType::EndOfFile, eof_loc())) {}

	Token expect(TokenType type);
	Token next();
	inline void consume() { next(); }
	const Token& peek(size_t skip = 0) const;
};
