#include "tokenizer.hpp"

#include <cctype>
#include <cstddef>
#include <stdexcept>

std::vector<Token> tokenize(const std::string& str) {
	std::vector<Token> result;
	Location current(0, 0);
	for (size_t s = 0; s < str.length();) {
		char c = str[s];
		if (c == '(') {
			result.push_back(Token(TokenType::LParen, current));
		} else if (c == ')') {
			result.push_back(Token(TokenType::RParen, current));
		} else if (c == '[') {
			result.push_back(Token(TokenType::LBracket, current));
		} else if (c == ']') {
			result.push_back(Token(TokenType::RBracket, current));
		} else if (c == '{') {
			result.push_back(Token(TokenType::LCurly, current));
		} else if (c == '}') {
			result.push_back(Token(TokenType::RCurly, current));
		} else if (c == '+') {
			result.push_back(Token(TokenType::Plus, current, c));
		} else if (c == '-') {
			result.push_back(Token(TokenType::Minus, current, c));
		} else if (c == '*') {
			result.push_back(Token(TokenType::Star, current, c));
		} else if (c == '/') {
			result.push_back(Token(TokenType::Slash, current, c));
		} else if (c == '=') {
			result.push_back(Token(TokenType::Equals, current));
		} else if (c == ',') {
			result.push_back(Token(TokenType::Comma, current));
		} else if (c == ';') {
			result.push_back(Token(TokenType::Semicolon, current));
		} else if (c == '\n') {
			// skip newline and increment line counter
			current.next_line();
			s++;
			continue;
		} else if (c == '\r') {
			// ignore \r because it should only be in front of \n
			s++;
			continue;
		} else if (std::isspace(c)) {
			// for all other types of space
			current.next_column();
			s++;
			continue;
		} else if (std::isalpha(c)) {
			Location start_pos = current;
			size_t start = s;
			current.next_column();
			s++;
			while (s < str.length()) {
				if (!std::isalnum(str[s])) break;
				current.next_column();
				s++;
			}
			std::string ident(str.begin() + start, str.begin() + s);
			TokenType type = TokenType::Identifier;
			if (ident == "let")
				type = TokenType::Let;
			else if (ident == "void")
				type = TokenType::Void;
			else if (ident == "if")
				type = TokenType::If;
			else if (ident == "for")
				type = TokenType::For;
			result.push_back(Token(type, start_pos, current - 1, ident));
			continue;
		} else if (std::isdigit(str[s])) {
			Location start_pos = current;
			size_t start = s;
			current.next_column();
			s++;
			while (s < str.length()) {
				if (!std::isdigit(str[s])) break;
				current.next_column();
				s++;
			}
			std::string num(str.begin() + start, str.begin() + s);
			result.push_back(Token(TokenType::Integer, start_pos, current - 1, static_cast<unsigned int>(std::stoi(num))));
			continue;
		} else {
			throw std::runtime_error("Illegal character `"s + str[s] + "` at "s + current.to_string());
		}
		current.next_column();
		s++;
	}
	return result;
}
