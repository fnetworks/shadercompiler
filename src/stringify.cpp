#include "stringify.hpp"
#include "ast.hpp"

void StringifyVisitor::visit_integer(const ast::Integer& n) {
	output << indent() << "Integer " << n.value();
}

void StringifyVisitor::visit_binary_op(const ast::BinaryOp& n) {
	output << indent() << "BinaryOp " << n.get_operator() << '\n';
	m_indent++;
	n.left()->dispatch(*this);
	output << '\n';
	n.right()->dispatch(*this);
	m_indent--;
}

void StringifyVisitor::visit_assignment(const ast::Assignment& n) {
	output << indent() << "Assignment\n";
	m_indent++;
	output << indent() << n.left() << '\n';
	n.right()->dispatch(*this);
	m_indent--;
}

void StringifyVisitor::visit_variable(const ast::Variable& n) {
	output << indent() << "Variable " << n.name();
}

void StringifyVisitor::visit_function_call(const ast::FunctionCall& n) {
	output << indent() << "FunctionCall " << n.name();
	m_indent++;
	for (const auto& arg : n.arguments()) {
		output << '\n';
		arg->dispatch(*this);
	}
	m_indent--;
}

void StringifyVisitor::visit_basic_block(const ast::BasicBlock& n) {
	output << indent() << "BasicBlock";
	m_indent++;
	for (const auto& stmt : n.statements()) {
		output << '\n';
		stmt->dispatch(*this);
	}
	m_indent--;
}

void StringifyVisitor::visit_variable_decl(const ast::VariableDecl& n) {
	output << indent() << "VariableDecl " << n.name();
	if (n.value()) {
		m_indent++;
		output << '\n';
		(*n.value())->dispatch(*this);
		m_indent--;
	}
}

void StringifyVisitor::visit_function_decl(const ast::FunctionDecl& n) {
	output << indent() << "FunctionDecl " << n.name() << '\n';
	m_indent++;
	n.content()->dispatch(*this);
	m_indent--;
}

void StringifyVisitor::visit_compilation_unit(const ast::CompilationUnit& n) {
	output << indent() << "CompilationUnit";
	m_indent++;
	for (const auto& fn : n.functions()) {
		output << '\n';
		fn->dispatch(*this);
	}
	m_indent--;
}
